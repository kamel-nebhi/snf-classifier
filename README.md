# snf-classifier

## Models

### SVM (tf-idf + 1gram & 2gram)
In this proposed categorization system, a support vector machine is used. 
In this classification model TFIDF (term frequency-inverse document frequency) weighting with chunk of unigrams and bigrams.

### BERT Siamese 
Our second model is similar to Siamese architecture. 
It takes two different data sources simultaneously in the same trainable transformer structure. 
The final hidden state is pooled with an average operation. The resulting concatenation is passed in a fully connected layer 
that combines them and produces probabilities.

## Performance

We have evaluated our models on a test set (33% of the original dataset) of 989 samples.
The performance are described in the table below.
Our SVM model 77% accuracy on our test data where our BERT Siamese model performs
better with 82% accuracy.


|             Models             | TestSet (Acc) |
|:------------------------------:|:-------------:|
|    SVM (tfidf/1gram/2gram)     |     0.77      | 
|          BERT Siamese          |     0.82      |


<details>
<summary>
SVM Classifier Performance Report
</summary>

|       | Precision | Recall | f1-score |
|:-----:|:---------:|:------:|:--------:|
|   0   |   0.79    |  0.73  |   0.76   |
|   1   |   0.76    |  0.81  |   0.78   |

</details>

<details>
<summary>
BERT Siamese Performance Report
</summary>

|       | Precision | Recall | f1-score |
|:-----:|:---------:|:------:|:--------:|
|   0   |   0.85    |  0.73  |   0.81   |
|   1   |   0.80    |  0.87  |   0.83   |

</details>
