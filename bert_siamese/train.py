import os
import random
import itertools
import numpy as np
import pandas as pd
from tqdm.notebook import tqdm
import torch

from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, confusion_matrix

import tensorflow as tf
from tensorflow.keras.layers import *
from tensorflow.keras.models import *
from tensorflow.keras.optimizers import *
from tensorflow.keras.callbacks import *
from tensorflow.keras import backend as K

from transformers import TFAutoModel, AutoTokenizer, BertConfig, TFBertModel

import logging
logging.basicConfig(level=logging.ERROR)

mapping = {
    False: 0,
    True: 1
}

# open training data
df = pd.read_csv("../data/labeled_data.csv", sep=",")

# drop columns null values
df['Abstract'].dropna(inplace=True)
df['Title'].dropna(inplace=True)
df['Keywords'].dropna(inplace=True)

df["Title_kw"] = df["Title"].astype(str) + df["Keywords"].astype(str)

df["is_interdisciplinary"] = df["is_interdisciplinary"].map(mapping)

df_to_tag = pd.read_csv("../data/unlabeled_data.csv", sep=",")

df_to_tag['Abstract'].dropna(inplace=True)
df_to_tag['Title'].dropna(inplace=True)
df_to_tag['Keywords'].dropna(inplace=True)
df_to_tag["Title_kw"] = df_to_tag["Title"].astype(str) + df_to_tag["Keywords"].astype(str)


# FUNCTIONS FOR TOKENIZATIONS, MASKS AND SEGMENTS CREATION ###

def set_seed(seed):
    tf.random.set_seed(seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    np.random.seed(seed)
    random.seed(seed)


def get_transformer_inputs(str1, str2, tokenizer, max_sequence_length, double=True):
    def return_id(str1, str2, truncation_strategy, length):

        # tokenize columns - make sure the sequence are always the same with truncation
        inputs = tokenizer.encode_plus(str1, str2,
                                       add_special_tokens=True,
                                       max_length=length,
                                       truncation_strategy=truncation_strategy,
                                       truncation=True
                                       )

        input_ids = inputs["input_ids"]
        input_masks = [1] * len(input_ids)
        input_segments = inputs["token_type_ids"]

        padding_length = length - len(input_ids)
        padding_id = tokenizer.pad_token_id

        input_ids = input_ids + ([padding_id] * padding_length)
        input_masks = input_masks + ([0] * padding_length)
        input_segments = input_segments + ([0] * padding_length)

        return [input_ids, input_masks, input_segments]

    if double:

        input_ids_1, input_masks_1, input_segments_1 = return_id(
            str1, None, 'longest_first', max_sequence_length)

        input_ids_2, input_masks_2, input_segments_2 = return_id(
            str2, None, 'longest_first', max_sequence_length)

        return [input_ids_1, input_masks_1, input_segments_1,
                input_ids_2, input_masks_2, input_segments_2]


def get_input_arrays(df, columns, tokenizer, max_sequence_length, double=True):
    input_ids_1, input_masks_1, input_segments_1 = [], [], []
    input_ids_2, input_masks_2, input_segments_2 = [], [], []
    for _, instance in tqdm(df[columns].iterrows(), total=len(df)):
        str1, str2 = instance[columns[0]], instance[columns[1]]

        ids_1, masks_1, segments_1, ids_2, masks_2, segments_2 = \
            get_transformer_inputs(str1, str2, tokenizer, max_sequence_length, double=double)

        input_ids_1.append(ids_1)
        input_masks_1.append(masks_1)
        input_segments_1.append(segments_1)

        input_ids_2.append(ids_2)
        input_masks_2.append(masks_2)
        input_segments_2.append(segments_2)

    if double:

        return [np.asarray(input_ids_1, dtype=np.int32),
                np.asarray(input_masks_1, dtype=np.int32),
                np.asarray(input_segments_1, dtype=np.int32),
                np.asarray(input_ids_2, dtype=np.int32),
                np.asarray(input_masks_2, dtype=np.int32),
                np.asarray(input_segments_2, dtype=np.int32)]


# split train and test
X_train, X_test, y_train, y_test = train_test_split(df[['Title_kw', 'Abstract']], df['is_interdisciplinary'].values,
                                                    random_state=42, test_size=0.33)

# set max length for truncation
MAX_SEQUENCE_LENGTH = 300

# start tokenization with bert-base-uncased model
print('Loading BERT tokenizer...')
tokenizer = AutoTokenizer.from_pretrained("bert-base-uncased", do_lower_case=True)
# tokenize train, test and unlabelled dataset
input_train = get_input_arrays(X_train, ['Title_kw', 'Abstract'], tokenizer, MAX_SEQUENCE_LENGTH)
input_test = get_input_arrays(X_test, ['Title_kw', 'Abstract'], tokenizer, MAX_SEQUENCE_LENGTH)
input_tag = get_input_arrays(df_to_tag, ['Title_kw', 'Abstract'], tokenizer, MAX_SEQUENCE_LENGTH)


# model creation
def siamese_bert():
    set_seed(42)

    # set up Adam optimization
    opt = Adam(learning_rate=2e-5)

    id1 = Input((MAX_SEQUENCE_LENGTH,), dtype=tf.int32)
    id2 = Input((MAX_SEQUENCE_LENGTH,), dtype=tf.int32)

    mask1 = Input((MAX_SEQUENCE_LENGTH,), dtype=tf.int32)
    mask2 = Input((MAX_SEQUENCE_LENGTH,), dtype=tf.int32)

    atn1 = Input((MAX_SEQUENCE_LENGTH,), dtype=tf.int32)
    atn2 = Input((MAX_SEQUENCE_LENGTH,), dtype=tf.int32)

    config = BertConfig()
    config.output_hidden_states = False  # Set to True to obtain hidden states
    bert_model = TFBertModel.from_pretrained('bert-base-uncased', config=config)
    # take the 2 output in bert
    embedding1 = bert_model(id1, attention_mask=mask1, token_type_ids=atn1)[0]
    embedding2 = bert_model(id2, attention_mask=mask2, token_type_ids=atn2)[0]

    # final hidden state is pooled
    with an average operation.
    x1 = GlobalAveragePooling1D()(embedding1)
    x2 = GlobalAveragePooling1D()(embedding2)

    # concatenation is passed in a fully connected layer
    # that combines them and produces probabilities
    x = Concatenate()([x1, x2])
    x = Dense(64, activation='relu')(x)
    x = Dropout(0.2)(x)
    out = Dense(2, activation='softmax')(x)

    model = Model(inputs=[id1, mask1, atn1, id2, mask2, atn2], outputs=out)
    model.compile(loss='sparse_categorical_crossentropy', optimizer=opt)

    return model


model = siamese_bert()
model.fit(input_train, y_train, epochs=3, batch_size=6)

# performance on test set
pred_test = np.argmax(model.predict(input_test), axis=1)
print(classification_report(y_test, pred_test))

# tag unlabeled dataset et store results in csv
df_to_tag["pred"] = np.argmax(model.predict(input_tag), axis=1)
print(df_to_tag["pred"].value_counts())
df_to_tag.to_csv("../data/output_unlabeled_data_bertSiamClass.csv", sep=',', encoding='utf-8', index=False)
