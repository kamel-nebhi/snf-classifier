import pandas as pd
from nltk.tokenize import word_tokenize
from nltk import pos_tag
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from collections import defaultdict
from nltk.corpus import wordnet as wn
import pickle5 as pickle

df = pd.read_csv("../data/unlabeled_data.csv", sep=",")

mapping = {
    0: False,
    1: True
}


def get_df_pre_processing(df):
    ## pre-processing
    # Step - a : Remove blank rows if any.
    df['Abstract'].dropna(inplace=True)
    df['Title'].dropna(inplace=True)
    df['Keywords'].dropna(inplace=True)

    # Step - b : Change all the text to lower case. This is required as python interprets 'dog' and 'DOG' differently
    df['Abstract'] = [abs.lower() for abs in df['Abstract']]
    df['Title'] = [abs.lower() for abs in df['Title']]
    df['Keywords'] = [abs.lower() for abs in df['Keywords'].astype(str)]

    # Step - c : Tokenization : In this each entry in the corpus will be broken into set of words
    df['Abstract'] = [word_tokenize(entry) for entry in df['Abstract']]
    df['Title'] = [word_tokenize(entry) for entry in df['Title']]
    df['Keywords'] = [word_tokenize(entry) for entry in df['Keywords']]

    # Step - d : Remove Stop words, Non-Numeric and perfom Word Stemming/Lemmenting.
    # WordNetLemmatizer requires Pos tags to understand if the
    # word is noun or verb or adjective etc. By default it is set to Noun
    tag_map = defaultdict(lambda: wn.NOUN)
    tag_map['J'] = wn.ADJ
    tag_map['V'] = wn.VERB
    tag_map['R'] = wn.ADV

    for index, entry in enumerate(df['Abstract']):
        # Declaring Empty List to store the words that follow the rules for this step
        Final_words = []
        # Initializing WordNetLemmatizer()
        word_Lemmatized = WordNetLemmatizer()
        # pos_tag function below will provide the 'tag' i.e if the word is Noun(N) or Verb(V) or something else.
        for word, tag in pos_tag(entry):
            # Below condition is to check for Stop words and consider only alphabets
            if word not in stopwords.words('english') and word.isalpha():
                word_Final = word_Lemmatized.lemmatize(word, tag_map[tag[0]])
                Final_words.append(word_Final)
        # The final processed set of words for each iteration will be stored in 'text_final'
        df.loc[index, 'abstract_final'] = str(Final_words)

    for index, entry in enumerate(df['Title']):
        # Declaring Empty List to store the words that follow the rules for this step
        Final_words = []
        # Initializing WordNetLemmatizer()
        word_Lemmatized = WordNetLemmatizer()
        # pos_tag function below will provide the 'tag' i.e if the word is Noun(N) or Verb(V) or something else.
        for word, tag in pos_tag(entry):
            # Below condition is to check for Stop words and consider only alphabets
            if word not in stopwords.words('english') and word.isalpha():
                word_Final = word_Lemmatized.lemmatize(word, tag_map[tag[0]])
                Final_words.append(word_Final)
        # The final processed set of words for each iteration will be stored in 'text_final'
        df.loc[index, 'title_final'] = str(Final_words)

    for index, entry in enumerate(df['Keywords']):
        # Declaring Empty List to store the words that follow the rules for this step
        Final_words = []
        # Initializing WordNetLemmatizer()
        word_Lemmatized = WordNetLemmatizer()
        # pos_tag function below will provide the 'tag' i.e if the word is Noun(N) or Verb(V) or something else.
        for word, tag in pos_tag(entry):
            # Below condition is to check for Stop words and consider only alphabets
            if word not in stopwords.words('english') and word.isalpha():
                word_Final = word_Lemmatized.lemmatize(word, tag_map[tag[0]])
                Final_words.append(word_Final)
        # The final processed set of words for each iteration will be stored in 'text_final'
        df.loc[index, 'keywords_final'] = str(Final_words)

    return df["abstract_final"], df["title_final"], df["keywords_final"]


df["abstract_final"], df["title_final"], df["keywords_final"] = get_df_pre_processing(df)

X = df[['abstract_final', 'keywords_final', 'title_final']]

model = pickle.load(open("model/svm_select.pkl", 'rb'))

print(model)

y_pred = model.predict(X)
df["pred"] = y_pred
df["pred"] = df["pred"].map(mapping)

print(df["pred"].value_counts())
df.to_csv("../data/output_unlabeled_data.csv", sep=',', encoding='utf-8', index=False)

