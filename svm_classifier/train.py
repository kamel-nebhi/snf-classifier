import pandas as pd
import numpy as np
from nltk.tokenize import word_tokenize
from nltk import pos_tag
from nltk.corpus import stopwords
from nltk.stem import WordNetLemmatizer
from collections import defaultdict
from nltk.corpus import wordnet as wn
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import model_selection, svm
from sklearn.feature_selection import SelectKBest, chi2
import pickle5 as pickle
from sklearn.metrics import classification_report
from sklearn.pipeline import Pipeline
from sklearn.compose import ColumnTransformer
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedKFold

mapping = {
    False: 0,
    True: 1
}


def get_df_pre_processing(df):
    ## pre-processing
    # Step - a : Remove blank rows if any.
    df['Abstract'].dropna(inplace=True)
    df['Title'].dropna(inplace=True)
    df['Keywords'].dropna(inplace=True)

    # Step - b : Change all the text to lower case. This is required as python interprets 'dog' and 'DOG' differently
    df['Abstract'] = [abs.lower() for abs in df['Abstract']]
    df['Title'] = [abs.lower() for abs in df['Title']]
    df['Keywords'] = [abs.lower() for abs in df['Keywords'].astype(str)]

    # Step - c : Tokenization : In this each entry in the corpus will be broken into set of words
    df['Abstract'] = [word_tokenize(entry) for entry in df['Abstract']]
    df['Title'] = [word_tokenize(entry) for entry in df['Title']]
    df['Keywords'] = [word_tokenize(entry) for entry in df['Keywords']]

    # Step - d : Remove Stop words, Non-Numeric and perfom Word Stemming/Lemmenting.
    # WordNetLemmatizer requires Pos tags to understand if the
    # word is noun or verb or adjective etc. By default it is set to Noun
    tag_map = defaultdict(lambda: wn.NOUN)
    tag_map['J'] = wn.ADJ
    tag_map['V'] = wn.VERB
    tag_map['R'] = wn.ADV

    for index, entry in enumerate(df['Abstract']):
        # Declaring Empty List to store the words that follow the rules for this step
        Final_words = []
        # Initializing WordNetLemmatizer()
        word_Lemmatized = WordNetLemmatizer()
        # pos_tag function below will provide the 'tag' i.e if the word is Noun(N) or Verb(V) or something else.
        for word, tag in pos_tag(entry):
            # Below condition is to check for Stop words and consider only alphabets
            if word not in stopwords.words('english') and word.isalpha():
                word_Final = word_Lemmatized.lemmatize(word, tag_map[tag[0]])
                Final_words.append(word_Final)
        # The final processed set of words for each iteration will be stored in 'text_final'
        df.loc[index, 'abstract_final'] = str(Final_words)

    for index, entry in enumerate(df['Title']):
        # Declaring Empty List to store the words that follow the rules for this step
        Final_words = []
        # Initializing WordNetLemmatizer()
        word_Lemmatized = WordNetLemmatizer()
        # pos_tag function below will provide the 'tag' i.e if the word is Noun(N) or Verb(V) or something else.
        for word, tag in pos_tag(entry):
            # Below condition is to check for Stop words and consider only alphabets
            if word not in stopwords.words('english') and word.isalpha():
                word_Final = word_Lemmatized.lemmatize(word, tag_map[tag[0]])
                Final_words.append(word_Final)
        # The final processed set of words for each iteration will be stored in 'text_final'
        df.loc[index, 'title_final'] = str(Final_words)

    for index, entry in enumerate(df['Keywords']):
        # Declaring Empty List to store the words that follow the rules for this step
        Final_words = []
        # Initializing WordNetLemmatizer()
        word_Lemmatized = WordNetLemmatizer()
        # pos_tag function below will provide the 'tag' i.e if the word is Noun(N) or Verb(V) or something else.
        for word, tag in pos_tag(entry):
            # Below condition is to check for Stop words and consider only alphabets
            if word not in stopwords.words('english') and word.isalpha():
                word_Final = word_Lemmatized.lemmatize(word, tag_map[tag[0]])
                Final_words.append(word_Final)
        # The final processed set of words for each iteration will be stored in 'text_final'
        df.loc[index, 'keywords_final'] = str(Final_words)

    return df["abstract_final"], df["title_final"], df["keywords_final"]


np.random.seed(500)

# read input file
df = pd.read_csv("../data/labeled_data.csv", sep=",")

# mapping target column for prediction
df["is_interdisciplinary"] = df["is_interdisciplinary"].map(mapping)

# nlp pre-preprocessing (lowercase, stopword, tokenization, lemmatization)
df["abstract_final"], df["title_final"], df["keywords_final"] = get_df_pre_processing(df)

# split train/test
X_train, X_test, y_train, y_test = model_selection.train_test_split(df[['abstract_final', 'keywords_final', 'title_final']],
                                                                    df['is_interdisciplinary'], test_size=0.33,
                                                                    random_state=42)

# initialise tf-idf vectorizers for 3 columns
vectorizer1 = TfidfVectorizer(ngram_range=(1, 2), min_df=5, norm='l2', sublinear_tf=True)
vectorizer2 = TfidfVectorizer(ngram_range=(1, 2), min_df=2, norm='l2', sublinear_tf=True)
vectorizer3 = TfidfVectorizer(ngram_range=(1, 2), min_df=2, norm='l2', sublinear_tf=True)

# create column transfomer
column_transformer = ColumnTransformer(transformers=
    [('tfidf1', vectorizer1, 'abstract_final'),
    ('tfidf2', vectorizer2, 'keywords_final'),
    ('tfidf3', vectorizer3, 'title_final')],
    remainder='passthrough')

# init svm linear
base_estimator = svm.SVC(kernel='linear')

# fit the model with feature selection based on chi2
pipeline = Pipeline([
                  ('tfidf', column_transformer),
                  ('feature_selection', SelectKBest(chi2, k=6000)),
                  ('base_estimator', base_estimator)
                ])

# grid search params
param_svm = {
    'base_estimator__C': [1, 2, 3],
    'base_estimator__degree': [1],
    'base_estimator__gamma': ['auto']
}

# init grid search and 5 fold cross validation
grid = GridSearchCV(
    pipeline,
    param_svm,
    cv=StratifiedKFold(n_splits=5, shuffle=True),
    verbose=2,
    refit=True
)

grid.fit(X_train, y_train)

# best score
print("Best parameter (CV score=%0.3f):" % grid.best_score_)
print(grid.best_params_)

# evalution and classification report on test set
cr = classification_report(y_test, grid.best_estimator_.predict(X_test))
print(cr)


# save output model
output_model = "model/svm_select.pkl"
pickle.dump(grid.best_estimator_, open(output_model, "wb"))
print("model saved==> ", output_model)

